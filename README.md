# Airflow investigation

## Prerequisites

* docker-compose
* cvmfs mounted to `/cvmfs`
* open ports on firewall 8080, 5000-5199, 5555

### How to mount cvmfs

It's easy to mount cvmfs on a CERN Puppet managed machine (like the ones on OpenStack using Cern CentOS Image).

```bash
$ locmap --enable cvmfs
$ locmap --configure cvmfs
```

And edit `/etc/cvmfs/default.local` and add `sft.cern.ch` to `CVMFS_REPOSITORIES`.
Now you should be able to access `/cvmfs/sft.cern.ch`. If this is not the case, try to reload the `autofs` service:

```bash
$ service autofs reload
```

### How to open ports

On CentOS machines just use `firewall-cmd` command, for example:

```bash
$ firewall-cmd --add-port=8080/tcp --permanent
$ firewall-cmd --reload
```

## Run


### Services

Docker compose starts 6 containers:

* **postgres**: used by airflow to persist data
* **redis**: used by celery as a MessageQueue to schedule tasks
* **airflow webserver**: airflow UI
* **airflow scheduler**: airflow celery main scheduler
* **airflow worker**: airflow celery worker
* **celery flower**: celery UI

### Build docker images

Two Docker images are needed. The first one is used by the following containers: 
* airflow webserver
* airflow scheduler
* celery flower

The second one is only used by the airflow worker container.

The difference is that in the worker's Docker image,  we source the `LCG-100` environment from `cvmfs` and we install all the dependencies using that environment.

To build the standard image:

```bash
$ cd build
$ chmod +x entrypoint
$ docker build -t airflowtest/main .
```

To build the worker image, you firstly need to have the `lhcsm`'s keytab. To do that, you need to use the `cern-get-keytab` command (inside `lxplus` for example). 

```bash
$ cern-get-keytab --user --keytab lhcsm.keytab --login lhcsm
```

Once you have `lhcsm.keytab`, place it in the `workerbuild` directory and then:

```bash
$ cd workerbuild
$ chmod +x entrypoint
$ docker build -t airflowtest/worker .
```

### Execution

During the first execution, only `airflow-init` container needs to be started. It executes database upgrades and it creates the `admin` account on airflow.

To execute it:

```bash
$ docker-compose up airflow-init
```

After it finishes, all the other containers can be executed:

```bash
$ docker-compose up
```

Airflow UI is accessible to `<IP>:8080`.

### Multiple workers

For now, multiple workers on the same machine are not supported (due to port conflict). But it's possible to execute an additional worker on another machine.

To do that, just execute the airflow-worker container:

```bash
$ docker run -d -p 5000-5199:5000-5199 -p 8793:8793 \
    -v ${PWD}/dags:/opt/airflow/dags \
    -v ${PWD}/logs:/opt/airflow/logs \
    -v ${PWD}/plugins:/opt/airflow/plugins \
    -v /cvmfs:/cvmfs:shared \
    -e SPARK_PORT=5001 \
    -e SPARK_BLOCKMANAGER_PORT=5101 \
    -e AIRFLOW__WEBSERVER__SECRET_KEY=password \
    -e AIRFLOW__CORE__EXECUTOR=CeleryExecutor \
    -e "AIRFLOW__CORE__SQL_ALCHEMY_CONN=postgresql+psycopg2://airflow:airflow@<MANAGER_IP>/airflow" \
    -e "AIRFLOW__CELERY__RESULT_BACKEND=db+postgresql://airflow:airflow@<MANAGER_IP>/airflow" \
    -e "AIRFLOW__CELERY__BROKER_URL=redis://:@<MANAGER_IP>:6379/0" \
    -u 0:0 \
    --hostname $HOSTNAME \
    airflowtest/worker /entrypoint
```

You should replace `MANAGER_IP` with the IP of the machine where all the other services are running.

#### Note

Note that `dags` directory should be synced between the two workers (by means of a shared filesystem or any other method).


### Where to place dags

Dags should be placed inside the `dags` folder, which will be mounted to `/opt/airflow/dags` inside the containers. An example of a dag can be seen in `sigmon_nb.py` file.

It downloads a notebook from internet, replaces `%matplotlib notebook` with `%matplotlib inline`, inserts the Spark configuration cell and then executes it. The resulting notebook will be placed inside the `/tmp` directory of the worker.

### Parallelism, queues, pools

Airflow supports multiple workers and multiple tasks can be executed on a single worker. Parallelism can be tuned using [Airflow env variables](https://airflow.apache.org/docs/apache-airflow/stable/configurations-ref.html). 

Airflow also supports the concepts of **queues** and **pools**. 

Queues are used to _specialize_ workers. For example we could have notebooks that needs more performance and notebooks that are not that demanding. For instance, we could have two workers: one on a **hi_performance** queue and one on a **low_performance** queue. Then, we could schedule dags related to demanding notebooks to the **hi_performance** queue and they would be executed on the first worker.

Pools are used to limit the amount of tasks of the same type running in parallel. This is useful to us because we have a limit on how many connections we can have at the same time to NxCALS.

### What still needs to be worked on

* Multiple workers on the same machine: this is not working at the moment because there would be conflict with the forwarded ports. It could also be useless to have multiple workers on the same machine given that a single worker can execute multiple tasks.
* Syncing `dags` directory: we need to find a way to sync `dags` folder on all the workers. Airflow suggests to use a shared file system or git (pulling from a repo every _x_ minutes, for example).
* Proper destination for output notebooks: for now output notebooks are saved to the `/tmp` folder of the worker that executed the task. We need to find a place where to put executed notebooks (eos?).
* Renovate keytab: it seems that `lhcsm.keytab` expires after a certain period of time. We need to find a way to renovate it (cron, external injecting service?).