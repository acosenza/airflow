import datetime as dt
import tempfile
import os
import platform

from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago

import papermill as pm
from nbformat import v4 as nbf
from papermill.engines import NotebookExecutionManager

import requests
from traitlets import config

default_args = {
    'owner': 'airflow',
    # 'retries': 1,
    # 'retry_delay': dt.timedelta(minutes=5),
}

def execute():
    url = 'https://gitlab.cern.ch/LHCData/lhc-sm-hwc/-/raw/dev/ipd/AN_IPD_FPA.ipynb?inline=false'
    r = requests.get(url, allow_redirects=True)

    notebook = nbf.reads(r.content)

    code = f'''if 'spark' not in locals() and 'spark' not in globals():
        import os
        from pyspark import SparkContext, SparkConf
        from pyspark.sql import SparkSession

        nxcals_jars = os.getenv('NXCALS_JARS')
        conf = SparkConf()
        conf.set('spark.master', 'yarn')
        conf.set("spark.driver.host", '{platform.node()}')
        conf.set('spark.driver.bindAddress', '0.0.0.0')
        conf.set('spark.driver.port', {os.environ['SPARK_PORT']})
        conf.set('spark.blockManager.port', {os.environ['SPARK_BLOCKMANAGER_PORT']})
        conf.set('spark.app.name', 'airflowlcg100-1')
        conf.set('spark.yarn.appMasterEnv.PYTHONPATH', os.getenv('PYTHONPATH'))
        conf.set('spark.yarn.executorEnv.PYTHONPATH', os.getenv('PYTHONPATH'))
        conf.set('spark.executorEnv.LD_LIBRARY_PATH', os.getenv('LD_LIBRARY_PATH'))
        conf.set('spark.yarn.appMasterEnv.LD_LIBRARY_PATH', os.getenv('LD_LIBRARY_PATH'))
        conf.set('spark.driver.extraClassPath', nxcals_jars)
        conf.set('spark.executor.extraClassPath', nxcals_jars)
        conf.set('spark.org.apache.hadoop.yarn.server.webproxy.amfilter.AmIpFilter.param.PROXY_HOSTS','ithdp1001.cern.ch,ithdp1005.cern.ch')
        conf.set('spark.yarn.access.hadoopFileSystems', 'nxcals')
        # conf.set('spark.driver.extraJavaOptions', '-Dservice.url=\"https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093\"')
        # conf.set('spark.kerberos.keytab', '/lhcsm.keytab')
        # conf.set('spark.kerberos.principal', 'lhcsm@CERN.ch')
        conf.set('spark.yarn.dist.files', '/lhcsm.keytab#lhcsm.keytab')
        conf.set('spark.driver.extraJavaOptions', '-Dservice.url=\"https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093\" -Dkerberos.principal=lhcsm -Dkerberos.keytab=./lhcsm.keytab')
        # conf.set('spark.sql.execution.arrow.enabled', 'true')
        # conf.set('spark.rdd.compress', 'True')
        # conf.set('spark.yarn.isPython', 'true')
        
        sc = SparkContext(conf=conf)
        spark = SparkSession(sc)
    '''
    config_cell = nbf.new_code_cell(code)

    config_cell.metadata.papermill = dict(
        exception=None,
        start_time=None,
        end_time=None,
        duration=None,
        status=NotebookExecutionManager.PENDING
    )

    nb_man = NotebookExecutionManager(notebook)

    nb_man.nb.cells.insert(0, config_cell)
    
    for cell in nb_man.nb.cells:
        if cell.cell_type == 'code':
            cell.source = cell.source.replace('%matplotlib notebook', '%matplotlib inline')

    output_nb = nbf.writes_json(nb_man.nb)

    notebook_file = tempfile.NamedTemporaryFile(mode='w+')
    notebook_file.write(output_nb)

    pm.execute_notebook(notebook_file.name, f'{notebook_file.name}_out.ipynb', {
        'circuit_name': 'RD1.L2',
        'timestamp_fgc': 1612443650160000000,
        'author': 'test',
        'is_automatic': True
    })

    notebook_file.close()

with DAG('sigmon_nb_test',
         default_args=default_args,
         start_date=days_ago(0)
        ) as dag:
    
    execute_task = PythonOperator(task_id='execute', python_callable=execute, pool='notebook')

execute_task